import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MessagesDialogBoxComponent } from '../messages-dialog-box/messages-dialog-box.component';
import { MessagePassingService } from '../message-passing/message-passing.service';
import { UserVO } from '../message-passing/user-vo';
import * as Stomp from 'stompjs';
import * as SockJS from 'sockjs-client';
import { StompClientService } from '../stomp-client.service';
import { disconnect } from 'process';
@Component({
  selector: 'app-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.scss']
})
export class MessageComponent implements OnInit {

  constructor(private dialog: MatDialog, private messageService: MessagePassingService,
    private stompClientService: StompClientService) { }

  @Output() checkMessageOpenEmiiter: EventEmitter<any> = new EventEmitter<any>();
  @Output() messageObject: EventEmitter<any> = new EventEmitter<any>();
  unReadMessageCount = 0;
  private serverUrl = '/messaging-service/socket';
  isLoaded = false;
  isCustomSocketOpened = false;
  show = false;
  private stompClient;
  userVO = new UserVO();
  openChatHistoryId: any;
  isMessageOpend = false;

  ngOnInit(): void {
    // console.log(this.stompClientService.stompClient);
    // this.connectSocketClient();
    if (this.stompClientService.stompClient && this.stompClientService.stompClient.connect === false) {
      console.log('reconnecting web socket');
      this.stompClientService.initializeWebSocketConnection();
    }
    this.checkMessageOpenEmiiter.emit(false);
    this.messageObject.emit(this);
    this.getLoggedUserDetails();
    this.getUnReadMessageCount();
    this.addSubscription();
  }

  addSubscription() {
    this.stompClientService.reconnecting.subscribe(data => {
      console.error('socket disconnected caught in message component ');
    });
  }


  // connectSocketClient() {
  //   if (this.stompClientService.stompClient && this.stompClientService.stompClient.connected === false
  //     && !this.stompClientService.isLoaded) {
  //     this.stompClientService.initializeWebSocketConnection();
  //   }
  // }

  getLoggedUserDetails() {
    // this.connectSocketClient();
    this.messageService.getLoggedInUserDetails().subscribe(data => {
      this.userVO = data;
      // this.connectSocketClient();
      setTimeout(() => {
        this.openSocket(this.userVO.userId);
      }, 3000);
    });
  }

  getToken() {
    return localStorage.getItem('token');
  }


  getHeader() {
    const httpOptions = {
      Authorization: this.getToken()
    };
    return httpOptions;
  }


  initializeWebSocketConnection() {
    const ws = new SockJS(this.serverUrl);
    this.stompClient = Stomp.over(ws);
    this.stompClient.debug = null;
    // this.stompClient.reconnect_delay = 5000;
    this.stompClient.connect(this.getHeader(), (frame) => {
      this.isLoaded = true;
      // this.openGlobalSocket();
      this.openSocket(this.userVO.userId);
      this.show = true;
    }, (error) => {
      console.log(error);
    });
  }

  openSocket(userId) {
    if (this.stompClientService.isLoaded) {
      this.isCustomSocketOpened = true;
      console.log('Custom socket opened in message component . . . . .');
      this.stompClientService.stompClient.subscribe('/socket-publisher/message-' + userId, (message) => {
        this.handleResult(message);
      }, this.getHeader());
    }
  }

  handleResult(message) {
    console.log('Recieving in messages in message component. . . . ');
    if (message.body && JSON.parse(message.body).fromId !== this.userVO.userId) {
      this.unReadMessageCount = this.unReadMessageCount + 1;
    }
  }


  getUnReadMessageCount() {
    this.messageService.getTotalUserMessageCount().subscribe(data => {
      this.unReadMessageCount = data;
    });
  }

  openMessages() {
    const messagingDialogBox = this.dialog.open(MessagesDialogBoxComponent, {
      disableClose: true,
      width: '1000px',
      height: '565px',
      autoFocus: false,
      panelClass: 'custom-dialog-container'
    });
    messagingDialogBox.updatePosition({ right: '10px', top: '5px' });
    messagingDialogBox.afterOpened().subscribe(data => {
      this.checkMessageOpenEmiiter.emit(true);
      this.isMessageOpend = true;
    });
    messagingDialogBox.afterClosed().subscribe(data => {
      this.checkMessageOpenEmiiter.emit(false);
      this.isMessageOpend = false;
    });

    messagingDialogBox.componentInstance.updateReadTimeEmiiter.subscribe(data => {
      if (data === true && this.isMessageOpend === true) {
        this.getUnReadMessageCount();
      }
    });

    messagingDialogBox.componentInstance.openedMessageHistoryId.subscribe(data => {
      this.openChatHistoryId = data;
    });
  }

}
