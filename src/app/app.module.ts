import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ConfirmationDialogComponent } from './confirmation-dialog/confirmation-dialog.component';
import { MessagePassingComponent } from './message-passing/message-passing.component';
import { MessageComponent } from './message/message.component';
import { MessagesDialogBoxComponent } from './messages-dialog-box/messages-dialog-box.component';
import { NotificationComponent } from './notification/notification.component';
import { NotificationsDialogBoxComponent } from './notifications-dialog-box/notifications-dialog-box.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { JwtInterceptor } from './jwt-interceptor';
import {DragDropModule} from '@angular/cdk/drag-drop';
import { PrintConfigurationComponent } from './print-configuration/print-configuration.component';
import { CartComponent } from './cart/cart.component';
import { YoroappsRenderingLibModule } from 'yoroapps-rendering-lib';
import {MessageNotificationModule} from 'message-notification';
import { MatRightSheetModule } from 'mat-right-sheet';

@NgModule({
  declarations: [
    AppComponent,
    ConfirmationDialogComponent, MessagePassingComponent, NotificationComponent, MessageComponent,
    MessagesDialogBoxComponent,
    NotificationsDialogBoxComponent,
    PrintConfigurationComponent,
    CartComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    FlexLayoutModule,
    DragDropModule,
    HttpClientModule,
    YoroappsRenderingLibModule,
    MessageNotificationModule,
    MatRightSheetModule
  ],
  providers: [{ provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },],
  entryComponents: [MessagesDialogBoxComponent, NotificationsDialogBoxComponent],
  bootstrap: [AppComponent]
})
export class AppModule { }
