import { EventEmitter, Injectable, Output } from '@angular/core';
import * as Stomp from 'stompjs';
import * as SockJS from 'sockjs-client';

@Injectable({
  providedIn: 'root'
})
export class StompClientService {
  ws: any;

  constructor() {
  }

  private serverUrl = '/messaging-service/socket';
  isLoaded = false;
  isCustomSocketOpened = false;
  show = false;
  public stompClient;
  retryCount = 0;
  @Output() reconnecting: EventEmitter<any> = new EventEmitter<any>();

  getToken() {
    return localStorage.getItem('token');
  }


  getHeader() {
    const httpOptions = {
      Authorization: this.getToken()
    };
    return httpOptions;
  }

  initializeWebSocketConnection() {
    this.ws = new SockJS(this.serverUrl);
    this.stompClient = Stomp.over(this.ws);
    this.stompClient.debug = null;
    this.stompClient.reconnect_delay = 5000;
    console.log('initializing sockets');
    if (this.stompClient.connected === false && this.retryCount < 5) {
      this.stompClient.connect(this.getHeader(),
        (frame) => {
          this.isLoaded = true;
          this.retryCount = 0;
          console.log('Socketed created in stopmclient service');
          this.show = true;
          this.reconnecting.emit(true);
          console.log(this.stompClient);
          console.log(this.ws);
        }, (error) => {
          console.error(error);
          console.error(this.retryCount);
          this.retryCount++;
          this.show = false;
          this.isLoaded = false;
          this.reconnecting.emit(false);
          setTimeout(() => {
            this.initializeWebSocketConnection(); }, 7000);
        });
    }
    return this.stompClient;
  }

  websocketDisconnection() {
    this.ws.onclose(() => {
      console.error('disconnected');
    });
  }
}
