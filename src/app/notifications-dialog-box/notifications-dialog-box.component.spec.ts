import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NotificationsDialogBoxComponent } from './notifications-dialog-box.component';

describe('NotificationsDialogBoxComponent', () => {
  let component: NotificationsDialogBoxComponent;
  let fixture: ComponentFixture<NotificationsDialogBoxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NotificationsDialogBoxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NotificationsDialogBoxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
