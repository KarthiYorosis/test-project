import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MessagePassingService } from './message-passing.service';
import * as Stomp from 'stompjs';
import * as SockJS from 'sockjs-client';
import { Message, MessageHistory } from './message-passing-vo';
import { MatDialog } from '@angular/material/dialog';
import { ConfirmationDialogComponent } from '../confirmation-dialog/confirmation-dialog.component';
import { MessageGroup } from './message-group-vo';
import { UserVO } from './user-vo';

@Component({
  selector: 'lib-message-passing',
  templateUrl: './message-passing.component.html',
  styleUrls: ['./message-passing.component.css']
})
export class MessagePassingComponent implements OnInit {

  constructor(private messageService: MessagePassingService,
    private fb: FormBuilder, private matDialog: MatDialog) { }

  private serverUrl = '/messaging-service/socket';
  isLoaded = false;
  isCustomSocketOpened = false;
  show = false;
  private stompClient;
  messagePassingForm: FormGroup;
  messages: Message[] = [];
  sessionId: any;
  transactionId: any;
  userVO = new UserVO();
  userVOList: UserVO[];
  messageHistoryVO = new MessageHistory();
  showUserChatHistory = false;
  chatHistoryUserId: any;
  messageGroupVO = new MessageGroup();
  messageGroupVOList: MessageGroup[];
  showGroupChatHistory = false;
  today = new Date();

  ngOnInit(): void {
    this.initializeMessagePassingForm();
    this.getLoggedUserDetails();
    this.initializeWebSocketConnection();
    this.getUsersList();
    this.getMessageGroupList();
    this.addUserNamesAutocompleteList();
  }

  initializeMessagePassingForm() {
    this.messagePassingForm = this.fb.group({
      fromId: ['', Validators.required],
      toId: [''],
      message: ['', Validators.required],
      groupId: [''],
      userName: [],
    });
  }

  getMessageGroupList() {
    this.messageService.getMessageGroupList().subscribe(data => {
      console.log(data);
      this.messageGroupVOList = data;
    });
  }

  addUserNamesAutocompleteList() {
    this.messagePassingForm.get('userName').valueChanges.subscribe(data => {
      if (data != null && data !== '') {
        this.messageService.getUsersAutocompleteList(data).subscribe(usersList => {
          this.userVOList = usersList;
        });
      } else {
        this.getUsersList();
      }
    });
  }

  getLoggedUserDetails() {
    this.messageService.getLoggedInUserDetails().subscribe(data => {
      this.userVO = data;
      this.messagePassingForm.get('fromId').setValue(this.userVO.userId);
    });
  }

  getUsersList() {
    this.messageService.getListOfUsers().subscribe(data => {
      this.userVOList = data;
    });
  }

  addUserToGroup() {
    const addUsersDialog = this.matDialog.open(ConfirmationDialogComponent, {
      disableClose: true,
      data: { type: 'users-autocomplete', chatId: this.chatHistoryUserId },
      width: '350px'
    });
    console.log(this.messageGroupVO.id);
    addUsersDialog.afterClosed().subscribe(data => {
      console.log(data);
      if (data !== false) {
        this.messageGroupVO.messageGroupUsersVOList = [];
        if (!this.messageGroupVO.id) {
          this.messageGroupVO.messageGroupUsersVOList.push({ id: null, userId: data });
          this.messageGroupVO.messageGroupUsersVOList.push({ id: null, userId: this.chatHistoryUserId });
          if (data !== this.userVO.userId) {
            this.messageGroupVO.messageGroupUsersVOList.push({ id: null, userId: this.userVO.userId });
          }
          this.createUpdateMessageGroups();
        } else {
          this.messageGroupVO.messageGroupUsersVOList.push({ id: null, userId: data });
          this.createUpdateMessageGroups();
        }
      }
    });
  }

  addUserToExistingGroup() {
    this.messageGroupVO.id = this.messageHistoryVO.userId;
    this.addUserToGroup();
  }

  createUpdateMessageGroups() {
    this.messageService.saveMessageGroup(this.messageGroupVO).subscribe((response: any) => {
      if (!this.messageGroupVO.id) {
        this.messageGroupVO.id = response.uuid;
        this.getMessageGroupList();
      }
    });
  }

  initializeWebSocketConnection() {
    const ws = new SockJS(this.serverUrl);
    this.stompClient = Stomp.over(ws);
    this.stompClient.connect({}, (frame) => {
      this.isLoaded = true;
      // this.openGlobalSocket();
      this.openSocket(this.userVO.userId);
      this.show = true;
    });
  }

  keydown(event) {
    console.log(event);
    this.sendMessageUsingSocket();
  }

  userProfile(str) {
    const assignee = str.split(' ');
    for (let i = 0; i < assignee.length; i++) {
      assignee[i] = assignee[i].charAt(0).toUpperCase();
    }
    return assignee.join('');
  }

  sendMessageUsingSocket() {
    console.log(this.messagePassingForm.valid);
    if (this.messagePassingForm.valid) {
      const message: Message = this.buildMessageVO();
      console.log('Sending . . . . ');
      this.stompClient.send('/socket-subscriber/send/message', {}, JSON.stringify(message));
    }
  }

  loadChatDetails(selectedUser: any, pageIndex) {
    this.showGroupChatHistory = false;
    this.showUserChatHistory = false;
    console.log('loading chat history. . . . . ');
    this.messagePassingForm.get('groupId').setValue(null);
    this.messagePassingForm.get('toId').setValue(selectedUser.userId);
    this.messageHistoryVO = new MessageHistory();
    this.messageGroupVO.id = null;
    this.messageService.getMessageHistory(selectedUser.userId, pageIndex).subscribe(user => {
      this.messageHistoryVO = user;
      this.chatHistoryUserId = this.messageHistoryVO.userId;
      selectedUser.unReadMessageCount = 0;
      this.showUserChatHistory = true;
    });
  }

  loadGroupChatDetails(messageGroup, pageIndex) {
    this.showUserChatHistory = false;
    this.showGroupChatHistory = false;
    console.log('loading group chat history. . . . . ');
    this.messagePassingForm.get('groupId').setValue(messageGroup.id);
    this.messagePassingForm.get('toId').setValue(null);
    this.messageHistoryVO = new MessageHistory();
    console.log(messageGroup);
    this.messageService.getGroupMessageHistory(messageGroup.id, pageIndex).subscribe(data => {
      this.messageHistoryVO = data;
      messageGroup.unReadMessageCount = 0;
      this.showGroupChatHistory = true;
    });
  }

  buildMessageVO() {
    return {
      message: this.messagePassingForm.get('message').value, id: null, readTime: null, createdOn: null,
      groupId: this.messagePassingForm.get('groupId').value,
      fromId: this.messagePassingForm.get('fromId').value, toId: this.messagePassingForm.get('toId').value
    };
  }


  openSocket(userId) {
    if (this.isLoaded) {
      this.isCustomSocketOpened = true;
      console.log('Custom socket opened  . . . . .');
      this.stompClient.subscribe('/socket-publisher/' + userId, (message) => {
        this.handleResult(message);
      });
    }
  }

  openGlobalSocket() {
    this.stompClient.subscribe('/socket-publisher/group', (message) => {
      this.handleResult(message);
    });
  }

  handleResult(message) {
    console.log('Recieving . . . . ');
    console.log(message.headers.destination);
    console.log(this.messageHistoryVO);
    if (message.body) {
      const messageResult: Message = JSON.parse(message.body);
      this.messages.push(messageResult);
      if (messageResult.groupId === null || messageResult.groupId === '') {
        if (this.chatHistoryUserId === messageResult.toId || this.chatHistoryUserId === messageResult.fromId) {
          messageResult.createdOn = this.today;
          this.messageHistoryVO.messageVOList.push(messageResult);
        }
        this.updateUnReadMessagesCount(messageResult.fromId);
        // this.showGroupChatHistory = false;
        // this.showUserChatHistory = true;
      } else if (messageResult.groupId) {
        if (this.messageHistoryVO.userId === messageResult.groupId) {
          this.messageHistoryVO.messageVOList.push(messageResult);
        }
        this.updateGroupMessagesCount(messageResult.groupId);
        // this.showUserChatHistory = false;
        // this.showGroupChatHistory = true;
      }
      this.messagePassingForm.get('message').setValue(null);
    }
  }

  updateGroupMessagesCount(groupId) {
    if (this.messageHistoryVO.userId !== groupId) {
      this.messageGroupVOList.filter(messageGroup => messageGroup.id === groupId).forEach(toGroup => {
        console.log(toGroup.groupUnreadMessageCount);
        if (!toGroup.groupUnreadMessageCount) {
          toGroup.groupUnreadMessageCount = 0;
        }
        toGroup.groupUnreadMessageCount = toGroup.groupUnreadMessageCount + 1;
      });
    }
  }

  updateUnReadMessagesCount(fromId) {
    console.log(this.messageHistoryVO.userId === fromId);
    if (fromId) {
      this.messageService.updateReadTimeOfMessage(fromId, this.userVO.userId).subscribe(data => {
        console.log(data);
      });
    } else {
      this.userVOList.filter(user => user.userId === fromId).forEach(user => {
        user.unReadMessageCount = user.unReadMessageCount + 1;
      });
    }
  }

}
