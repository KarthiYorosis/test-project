import { STEPPER_GLOBAL_OPTIONS } from '@angular/cdk/stepper';
import { ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatVerticalStepper } from '@angular/material/stepper';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css'],
  providers: [{
    provide: STEPPER_GLOBAL_OPTIONS, useValue: { showError: true }
  }]
})
export class CartComponent implements OnInit {

  constructor(private formBuilder: FormBuilder, private cd: ChangeDetectorRef) { }

  @ViewChild(MatVerticalStepper) stepper: MatVerticalStepper;
  stepperFormGroup: FormGroup;
  steps = [
    { label: 'One', fields: [{ name: 'one', label: 'One', value: '' }], nextStepIndex: 1 },
    { label: 'Two', fields: [{ name: 'two', label: 'Two', value: '' }], nextStepIndex: 2 },
    { label: 'Three', fields: [{ name: 'three', label: 'Three', value: '' }], nextStepIndex: 3 },
  ];
  formArray = this.formBuilder.array([]);
  cardItems = [];
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  group = this.formBuilder.group({});
  showAddItemButton = true;
  updateItemIndex = -1;
  cardDetailsVO: any;
  toppingList: string[] = ['Extra cheese', 'Mushroom', 'Onion', 'Pepperoni', 'Sausage', 'Tomato'];

  ngOnInit() {
    this.stepperFormGroup = this.getFormGroupForCart();
    this.firstFormGroup = this.formBuilder.group({
      firstCtrl: ['', Validators.required]
    });
    this.secondFormGroup = this.formBuilder.group({
      secondCtrl: ['', Validators.required]
    });
    this.cardDetailsVO = this.getDetailsForCart();
    this.createFormArray();
    console.log(this.stepper)
  }

  getFormGroup() {
    return this.formBuilder.group({
      one: ['', Validators.required],
      two: ['', Validators.required],
      three: ['', Validators.required]
    });
  }

  addToCart() {
    if (this.stepperFormGroup.valid) {
      this.getFormArray().push(this.getFormGroupForCart());
      const json = this.stepperFormGroup.value
      this.getFormArray().get(this.getFormArray().length - 1 + '').setValue(json);
      this.addCartItems(json, this.stepperFormGroup, this.getFormArray().length - 1);
      this.stepper.selectedIndex = 0;
      this.stepperFormGroup.reset();
    }
  }

  nextStep(step,fieldValue) {
    step.stepValues.forEach(value => {
      if((value.key === fieldValue || value.value === fieldValue)&&value.hasNextStep){
       this.stepper.selectedIndex = value.nextStepIndex;
      }
    });
    if(step.controlType==='select'){

    }
  }

  addCartItems(json, formGroup: FormGroup, i: number) {
    let array: any[] = [];
    this.getDetailsForCart().stepDetails.forEach(step=>{
      Object.keys(json).forEach(element => {
        if (step.controlType.controlTypeName === element) {
          const value = formGroup.get(element).value;
          array.push({ name: step.controlType.controlTypeName, label: step.controlType.controlTypeLabel, value: value });
        }
      })
    })
    if (this.cardItems.some(item => item.index === i)) {
      this.cardItems.splice(i, 1, { index: i, details: array })
    } else {
      this.cardItems.push({ index: i, details: array });
    }
    array = []
  }

  edit(index) {
    this.showAddItemButton = false;
    this.stepperFormGroup.setValue(this.getFormArray().get(index + '').value);
    this.stepper.selectedIndex = 0;
    this.updateItemIndex = index;
  }

  updateItem(index) {
    this.showAddItemButton = false;
    if (this.stepperFormGroup.valid) {
      const json = this.stepperFormGroup.value
      this.getFormArray().get(index + '').setValue(json);
      this.addCartItems(this.stepperFormGroup.value, this.stepperFormGroup, index);
      this.stepper.selectedIndex = 0;
      this.stepperFormGroup.reset();
      this.updateItemIndex = -1;
      this.showAddItemButton = true;
    }

  }

  closeItem(index, itemIndex) {
    this.cardItems.splice(itemIndex, 1);
    this.getFormArray().removeAt(index);
    for (let i = 0; i < this.cardItems.length; i++) {
      this.cardItems[i].index = i;
    }
  }

  getFormGroupForCart(){
    const group = this.formBuilder.group({});
    this.getDetailsForCart().stepDetails.forEach(stepDetail=>{
      group.addControl(stepDetail.controlType.controlTypeName,this.formBuilder.control(''));
    })
    return group;
  }

  createFormArray(){
    this.group.addControl(this.getDetailsForCart().shoppingCartName,this.formBuilder.array([]));
  }

  getFormArray(){
    return this.group.get(this.getDetailsForCart().shoppingCartName) as FormArray;
  }

  getImageCardJson(){
    return {
      "controlType":"imagegrid",
      "childSection":false,
      "field":{
         "name":"gridImage",
         "fieldId":null,
         "fieldName":null,
         "defaultValue":"",
         "defaultCode":"",
         "control":{
            "imageGridLabel":"Grid Image",
            "imageGridName":"gridImage",
            "noOfRows":2,
            "noOfColumns":2,
            "height":200,
            "width":300,
            "position":"left",
            "rowLevelSpace":20,
            "columnLevelSpace":20,
            "showImageGridLabel":false,
            "optionType":"d",
            "filter":{
               "pageName":"New Image Page",
               "tableName":"xppm_newimagepage",
               "keyColumnName":"ya_uploadimage",
               "descriptionColumnName":"ya_imagetype",
               "sortOption":false,
               "sortBy":[
                  
               ],
               "loadFirstOption":false,
               "filterOptions":"",
               "joinClause":"",
               "version":1
            }
         },
         "label":{
            "labelName":null,
            "labelOption":"floating",
            "labelSize":null,
            "labelStyle":null,
            "labelPosition":null
         },
         "dataType":null,
         "fieldWidth":100,
         "unique":false,
         "editable":true,
         "sensitive":false,
         "enableHyperlink":false,
         "dateFormat":null,
         "rows":0,
         "cols":0,
         "chipSize":0,
         "dateValidation":{
            "dateValidation":false,
            "allowFutureDate":false,
            "operator":null,
            "fromField":null,
            "toField":null
         },
         "numberFieldValidation":{
            "numberFieldValidation":false,
            "operator":null,
            "fromField":null,
            "toField":null
         },
         "allowFutureDate":false,
         "allowPastDate":false,
         "onSelection":{
            "onSelectionChange":false,
            "fieldType":"",
            "loadDataLabel":null,
            "targetPageName":null,
            "targetPageId":null,
            "passParameter":null,
            "pageType":null,
            "version":null
         },
         "validations":[
            {
               "type":null,
               "value":null
            }
         ],
         "conditionalChecks":{
            "enable":{
               "option":false,
               "fields":[
                  
               ]
            },
            "show":{
               "option":false,
               "fields":[
                  
               ]
            },
            "required":{
               "option":false,
               "fields":[
                  
               ]
            }
         },
         "style":null,
         "rowBackground":"#ffffff"
      }
   }
  }
  
  getCardJson(){
    return {
      "controlType":"card",
      "childSection":false,
      "field":{
         "name":"staticCard",
         "fieldId":null,
         "fieldName":null,
         "defaultValue":null,
         "defaultCode":null,
         "control":{
            "borderColor":"#0078ff",
            "hoverColor":"#0078ff",
            "noOfRows":5,
            "noOfColumns":5,
            "optionType":"s",
            "optionsValues":[
               {
                  "code":"test",
                  "description":"test"
               },
               {
                  "code":"test 1",
                  "description":"test 1"
               }
            ]
         },
         "label":{
            "labelName":"Static Card",
            "labelOption":"floating",
            "labelSize":null,
            "labelStyle":null,
            "labelPosition":null
         },
         "dataType":null,
         "fieldWidth":100,
         "unique":false,
         "editable":true,
         "sensitive":false,
         "enableHyperlink":false,
         "dateFormat":null,
         "rows":0,
         "cols":0,
         "chipSize":0,
         "dateValidation":{
            "dateValidation":false,
            "allowFutureDate":false,
            "operator":null,
            "fromField":null,
            "toField":null
         },
         "numberFieldValidation":{
            "numberFieldValidation":false,
            "operator":null,
            "fromField":null,
            "toField":null
         },
         "allowFutureDate":false,
         "allowPastDate":false,
         "onSelection":{
            "onSelectionChange":false,
            "fieldType":"",
            "loadDataLabel":null,
            "targetPageName":null,
            "targetPageId":null,
            "passParameter":null,
            "pageType":null,
            "version":null
         },
         "validations":[
            {
               "type":null,
               "value":null
            }
         ],
         "conditionalChecks":{
            "enable":{
               "option":false,
               "fields":[
                  
               ]
            },
            "show":{
               "option":false,
               "fields":[
                  
               ]
            },
            "required":{
               "option":false,
               "fields":[
                  
               ]
            }
         },
         "style":null,
         "rowBackground":"#ffffff"
      }
   }
  }
  
  getDetailsForCart(){
    return {
      "shoppingCartName":"flipCart",
      "shoppingCartLabel":"Flip Cart",
      "stepsInvolved":10,
      "nameOfSteps":[
         
      ],
      "stepDetails":[
         {
            "stepName":"firstStep",
            "stepLabel":"First Step",
            "stepIndex":0,
            "minimumSelection":2,
            "maximumSelection":4,
            "controlType":{
               "controlType":"text",
               "controlTypeLabel":"First Step Control",
               "controlTypeName":"firstStepControl"
            },
            "stepValues":[
               {
                  "key":"",
                  "value":"tt",
                  "pricing":20,
                  "pricingType":"image",
                  "quantity":40,
                  "hasNextStep":true,
                  "nextStepName":"secondStep",
                  "nextStepIndex":3,
                  "nextStepMinimumSelection":3,
                  "nextStepMaximumSelection":6
               }
            ]
         },
         {
            "stepName":"secondStep",
            "stepLabel":"Second Step",
            "stepIndex":1,
            "minimumSelection":2,
            "maximumSelection":4,
            "controlType":{
               "controlType":"card",
               "controlTypeLabel":"Second Step Control",
               "controlTypeName":"secondStepControl"
            },
            "stepValues":[
               {
                  "key":"",
                  "value":"",
                  "pricing":20,
                  "pricingType":"image",
                  "quantity":40,
                  "hasNextStep":true,
                  "nextStepName":"",
                  "nextStepIndex":2,
                  "nextStepMinimumSelection":3,
                  "nextStepMaximumSelection":6
               }
            ]
         },
         {
            "stepName":"thirdStep",
            "stepLabel":"Third Step",
            "stepIndex":2,
            "minimumSelection":2,
            "maximumSelection":4,
            "controlType":{
               "controlType":"textArea",
               "controlTypeLabel":"Third Step Control",
               "controlTypeName":"thirdStepControl"
            },
            "stepValues":[
               {
                  "key":"",
                  "value":"",
                  "pricing":20,
                  "pricingType":"image",
                  "quantity":40,
                  "hasNextStep":true,
                  "nextStepName":"",
                  "nextStepIndex":2,
                  "nextStepMinimumSelection":3,
                  "nextStepMaximumSelection":6
               }
            ]
         },
         {
            "stepName":"fourthStep",
            "stepLabel":"Fourth Step",
            "stepIndex":3,
            "minimumSelection":2,
            "maximumSelection":4,
            "controlType":{
               "controlType":"select",
               "controlTypeLabel":"Fourth Step Control",
               "controlTypeName":"fourStepControl"
            },
            "stepValues":[
               {
                  "key":"one",
                  "value":"One",
                  "pricing":20,
                  "pricingType":"image",
                  "quantity":40,
                  "hasNextStep":true,
                  "nextStepName":"",
                  "nextStepIndex":4,
                  "nextStepMinimumSelection":3,
                  "nextStepMaximumSelection":6
               },
               {
                "key":"two",
                "value":"Two",
                "pricing":20,
                "pricingType":"image",
                "quantity":40,
                "hasNextStep":true,
                "nextStepName":"",
                "nextStepIndex":4,
                "nextStepMinimumSelection":3,
                "nextStepMaximumSelection":6
             },
             {
              "key":"three",
              "value":"Three",
              "pricing":20,
              "pricingType":"image",
              "quantity":40,
              "hasNextStep":true,
              "nextStepName":"",
              "nextStepIndex":4,
              "nextStepMinimumSelection":3,
              "nextStepMaximumSelection":6
           },  {
            "key":"four",
            "value":"Four",
            "pricing":20,
            "pricingType":"image",
            "quantity":40,
            "hasNextStep":true,
            "nextStepName":"",
            "nextStepIndex":4,
            "nextStepMinimumSelection":3,
            "nextStepMaximumSelection":6
         }
            ]
         },
         {
            "stepName":"fifthStep",
            "stepLabel":"Fifth Step",
            "stepIndex":5,
            "minimumSelection":2,
            "maximumSelection":4,
            "controlType":{
               "controlType":"image",
               "controlTypeLabel":"Fifth Step Control",
               "controlTypeName":"fifthStepControl"
            },
            "stepValues":[
               {
                  "key":"",
                  "value":"",
                  "pricing":20,
                  "pricingType":"image",
                  "quantity":40,
                  "hasNextStep":true,
                  "nextStepName":"",
                  "nextStepIndex":2,
                  "nextStepMinimumSelection":3,
                  "nextStepMaximumSelection":6
               }
            ]
         }
      ]
   }
  }
}
