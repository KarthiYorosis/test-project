export class ShoppingVo {
    shoppingCartName: string;
    shoppingCartLabel: string;
    stepsInvolved: number;
    stepDetails: StepDetails[];
    nameOfSteps: any[];
}
 
export class NameOfSteps {
    stepName: string;
    stepLabel: string;
    stepIndex: number;
}
 
export class StepDetails {
    stepName: string;
    stepLabel: string;
    stepIndex: number;
    minimumSelection: number;
    maximumSelection: number;
    controlType: ControlType;
    stepValues: any[];
}
 
export class ControlType {
    controlType: string;
    controlTypeLabel: string;
    controlTypeName: number;
}
 
export class StepValues {
    key: string;
    value: string;
    description: string;
    image: any;
    pricing: any;
    pricingType: string;
    quantity: number;
    hasNextStep: boolean;
    nextStepName: string;
    nextStepIndex: number;
    nextStepMinimumSelection: number;
    nextStepMaximumSelection: number;
}
