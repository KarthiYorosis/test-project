import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { HttpClient } from '@angular/common/http';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DateAdapter } from '@angular/material/core';

import { CustomDateAdapter } from './custom-date-adapter';
import { DateFormatService } from './date-format-service';

import { StompClientService } from './stomp-client.service';
export interface Vegetable {
  name: string;
}
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {
  title = 'test-project';
  constructor(private fb: FormBuilder, private df: DateFormatService, private st: StompClientService,
    private http: HttpClient) { }
  form: FormGroup;
  @ViewChild('sidenav') sidenave: any;
  end = 'left';
  json = [
    {
      controlType: 'input',
      field: {
        name: 'numberField1',
        dataType: 'long',
        required: true,
        editable: true,
        label: {
          labelName: 'Number Field 1'
        },
        dateFormat: '',
        control: {
          columnWidth: 40,
          columnHeaderAlignment: 'start start',
          textFieldType: 'textField'
        }
      }
    },
    {
      controlType: 'date',
      field: {
        name: 'numberField2',
        dataType: 'date',
        required: false,
        editable: true,
        label: {
          labelName: 'Number Field 2'
        },
        dateFormat: 'MM/DD/yyyy',
        control: {
          columnWidth: 20,
          columnHeaderAlignment: 'start start',
          textFieldType: 'textField'
        }
      }
    },
    {
      controlType: 'textarea',
      field: {
        name: 'numberFiled3',
        dataType: 'string',
        required: false,
        editable: true,
        label: {
          labelName: 'Number Filed 3'
        },
        dateFormat: '',
        control: {
          columnWidth: 25,
          columnHeaderAlignment: 'start start',
          textFieldType: 'textarea'
        }
      }
    },
    {
      controlType: 'input',
      field: {
        name: 'numberFiled4',
        dataType: 'float',
        required: false,
        editable: true,
        label: {
          labelName: 'Number Filed 4'
        },
        dateFormat: '',
        control: {
          columnWidth: 30,
          columnHeaderAlignment: 'start start',
          textFieldType: 'textField'
        }
      }
    }
  ];
  color = null;
  css = {
    /* your own custom styles here */
    /* e.g. */
    'color': 'red',
    'background-color': 'red'
  }
  @ViewChild('#tooltip') toolTip: any;
  userId = 'karthi';

  images: any[][] = [];
  testNames = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15,16,17,18,19,20];
  rows = 4;
  columns = 5;

  ngOnInit() {
    // this.st.initializeWebSocketConnection();
    console.log();
    // this.buildDataForImageGrid();
    this.form = this.fb.group({
      name: ['', [Validators.required]]
    });
    // tslint:disable-next-line: max-line-length
    localStorage.setItem('token', 'Bearer eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJhZG1pbkB5b3JvZmxvdy5jb20iLCJzY29wZXMiOiJST0xFX1VTRVIiLCJzdWJfZG9tYWluIjoieW9yb3Npcy55b3JvZmxvdy5jb20iLCJ1c2VyX3JvbGUiOlsid29ya2Zsb3ciLCJteS1kb25lLXRhc2siLCJteS1wZW5kaW5nLXRhc2siLCJteS1sYXVuY2hlZC10YXNrIiwiZ3JvdXAiLCJ1c2VyLWdyb3VwLWFzc29jaWF0aW9uIiwidXNlci1tYW5hZ2VtZW50IiwiZ2VuZXJhdGUtcmVwb3J0cyIsImNoYW5nZS1wYXNzd29yZCIsInByb2Nlc3MtaW5zdGFuY2UtY29tcGxldGVkLWxpc3QiLCJwcm9jZXNzLWluc3RhbmNlLWZhaWxlZC1saXN0IiwicHJvY2Vzcy1pbnN0YW5jZS1ydW5uaW5nLWxpc3QiLCJtZW51LWNvbmZpZyIsImdyaWQtY29uZmlnIiwiYXBwL2NyZWF0ZSIsInZpZXctdGFzay1saXN0IiwiY3JlYXRlIiwiZWRpdCIsIm9yZyIsImJvYXJkIiwibGluZSIsIm15LXBlbmRpbmctdGFzayIsImdldC9wYWdlIiwiYXBwL2VkaXQiLCJsYXlvdXQiLCJhcHAtbGF5b3V0IiwiZ2V0L2FwcC1sYXlvdXQtcGFnZSIsImFwcC1sYXlvdXQtcGFnZS1saXN0IiwiYXBwbGljYXRpb24tZGFzaGJvYXJkIiwidXNlci1wcm9maWxlIiwidGFibGUtb2JqZWN0IiwibWVzc2FnZSIsInVwZGF0ZS1vcmciLCJjcmVhdGUtb3JnIiwid29ya2Zsb3ctYXBwbGljYXRpb24iLCJkYXNoYm9hcmQiLCJlbWFpbC10ZW1wbGF0ZSIsImVtYWlsLXRlbXBsYXRlIiwidXNlci11cGRhdGUtb3JnIiwiY3VzdG9tLXBhZ2UiLCJ3b3JrZmxvdy1wYWdlIiwid29ya2Zsb3ctcGFnZS1saXN0IiwicGFnZS1saXN0IiwiY3JlYXRlL3BhZ2UiLCJoeXBlcmxpbmsiLCJpbnNlcnRkYnRhc2siLCJ3ZWJzZXJ2aWNlIiwid2Vic2VydmljZWdyb3VwIiwiZmFpbGVkdGFzayIsInVzZXJmb3JtIiwidGVzdF9mb3JtIiwidXBsb2FkY29udHJvbHRlc3QiLCJ0ZXN0Zm9ybSIsInRlc3Rmb3JtIiwiZGVtb2Zvcm0iLCJ0ZXN0Y2FsbHdvcmtmbG93IiwiZW1iZWRlZHBhZ2UiLCJwYXNzd29yZHBhZ2UiLCJzYXZlYW5kY2FsbHdvcmtmbG93IiwidGVzdCIsImRidGVzdCIsInBvcm9jZWR1cmVjb2RlIiwicGFzc3dvcmR0ZXN0MSIsImRhdGV0ZXN0a2FydGhpIiwiZm9ybXVwZGF0ZSIsImNoaXB0ZXN0IiwiZGF0ZXRlc3QiLCJjc3N0ZXN0a2FydGhpIiwidGVzdHNlbGVjdCIsImFwcGxpY2F0aW9ubGF5b3V0dGVzdCIsImFwcGxheW91dGthcnRoaXRlc3QiLCJ0ZXN0Y2hpcHJlcXVpcmVkIiwiZGF0ZWZvcm1hdGthcnRoaSIsInBhc3N3b3JkdGVzdCIsInlvcm9mbG93X3Rlc3QiLCJndmd2IiwiZGVtb2Zvcm0iLCJsaWNlbnNlcXVhbGlmaWVyIiwiZGVtb3dvcmtmbG93IiwiZmlsdGVydGVzdCIsImZpbGVhbmRzaWduYXR1cmUiLCJ0ZXN0Y2hpcCIsInRlc3RvbnNlbGVjdCIsInNkc2MiLCJmbG93Iiwidjd5aDc2eTciLCJ0ZXN0c2VsZWN0YW5kaHlwZXJsaW5rIiwiZ3JpZHRlc3QiLCJ6eGN6YyIsInVzZXJhZGRyZXNzIiwidXNlcmpvYiIsInRlc3RidXR0b25hY3Rpb24iLCJ0ZXN0Zm9ybSIsInRhYmJlZG1lbnUiLCJ0ZXN0ZGhpbmVzaCIsInRlc3RjaGlwcmVsb2FkIiwidGVzdHRhYmJlZGhvcml6b250YWwiLCJ0ZXN0X2Zvcm1fMSIsInRlc3RhbGxjb250cm9scyIsInRlc3R1cGRhdGVjaGlwIiwidGVzdHJhZGlvYnV0dG9uIiwiZGVsZXRlZGF0YSIsInRlc3RudW1iZXIiLCJ0ZXN0cGRmIiwibWFudWFsdmV0dGluZ2Zvcm0iLCJtYW51YWx2ZXR0aW5nZm9ybSIsIm1hbnVhbHZldHRpbmdmb3JtIiwibWFudWFsdmV0dGluZ2Zvcm0iLCJ3b3JrZmxvd2Zvcm0iLCJkZW1vdGVzdCIsInRlc3RzZWN0aW9uc2VjdXJpdHkiLCJmbG93IiwiZmlsdGVydGVzdCIsImZpbHRlcnRlc3QiLCJ0ZXN0Z3JpZGthcnRoaSIsInRlc3RzZWN0aW9uc2VjdXJpdHkiLCJ0aW1lc2hlZXRtb2RlbCIsInRpbWVzaGVldG1vZGVsIiwidGVzdHRhYmxlY29tcHV0ZSIsInVzZXJwcm9maWxlIiwidXNlcnByb2ZpbGUiLCJkZW1vZm9ybSIsImRlbW9mb3JtIiwibmVzdGVkc2VjdGlvbndvcmtmbG93IiwibGljZW5zZXF1YWxpZmllciIsIm5lc3RlZHNlY3Rpb24iLCJuZXN0ZWRzZWN0aW9uIiwiY29kZWFuZGRlc2NyaXB0aW9uIiwiY29kZWFuZGRlc2NyaXB0aW9uIiwidGVzdHRhYmxlIiwidGFibGVjb21wdXRhdGlvbjEiLCJhcnJheW11bHRpIiwiYXJyYXltdWx0aSIsImRlbW93b3JrZmxvdyIsInRhYmxlY29udHJvbCIsInRhYmxlY29udHJvbCIsIndvcmtmbG93dGFibGUiLCJ3b3JrZmxvd3RhYmxlIiwidGltZXNoZWV0IiwidGltZXNoZWV0IiwidGVzdG5vcm1hbHBhZ2UiLCJ0ZXN0cmVwZWF0YWJsZWZvcm1hcnJheSIsInRlc3R0YWJsZWNvbnRyb2wiLCJ0ZXN0dGFibGVjb250cm9sdHlwZSIsInRlc3Rjb250cm9sZm9ybXVsdGlsZWFycmF5IiwiZW1wbG95ZWVuYW1lIiwiZW1wbG95ZWVuYW1lIiwidGltZXNoZWV0bW9kZWwiLCJ0aW1lc2hlZXRtb2RlbCIsInRlc3R1c2Vyc3BlY2lmaWNncmlkIiwidGVzdHN0YXJ0IiwidGVzdHN0YXJ0IiwidGVzdHdvcmtmbG93c3VibWl0IiwidGVzdHdvcmtmbG93c3VibWl0IiwidGltZXNoZWV0bW9kZWwiLCJ0aW1lc2hlZXRtb2RlbCIsInRpbWVzaGVldG1vZGVsIiwiZGVtb2Zvcm0iLCJ0aW1lc2hlZXRtb2RlbCIsInB1YmxpY2FjY2Vzc2Zvcm0iLCJwdWJsaWNhY2Nlc3Nmb3JtIiwidGVzdHB1YmxpY2FjY2VzcyIsInRlc3RwdWJsaWNhY2Nlc3MiLCJwdWJsaWNhY2Nlc3Nmb3JtIiwicHVibGljYWNjZXNzZm9ybSIsImRlbW9mb3JtIiwibmVzdGVkc2VjdGlvbndvcmtmbG93IiwibmVzdGVkc2VjdGlvbndvcmtmbG93IiwidGVzdHdvcmtmbG93Zm9ybXdvcmtmbG93IiwidGVzdHdvcmtmbG93Zm9ybXdvcmtmbG93IiwidGVzdDEzNjM3Mzc3MzMiLCJ0ZXN0bmV3cGFnZTEiLCJ0ZXN0bmV3Zm9ybTEiLCJkZW1vZm9ybSIsImRlbW9mb3JtIiwiZGVtb2Zvcm0iLCJzdWJzZWN0aW9uZm9ybSIsInN1YnNlY3Rpb25mb3JtIl0sInRlbmFudF9pZCI6Inlvcm9mbG93IiwiaWF0IjoxNjA0NDAzOTQ3LCJleHAiOjE2MDQ0MjE5NDd9.uODya8hnl2YonV3pwbCU6DAIu6CloaVUe0gkTpmsx40');
  }

  buildDataForImageGrid() {

    for (let i = 0; i < this.rows; i++) {
      let test: any[] = []
      for (let j = 0; j < this.testNames.length; j++) {
        if (i === 0 && j < this.columns) {
          test.push(this.testNames[j]);
        }
        if(i>0){
          const start = i * this.columns;
          const end = (i * this.columns) + this.columns;
          if (j >= start && j < end) {
            test.push(this.testNames[j]);
          }
        }
      
      }
      this.images.push(test);
      test = [];
    }
    console.log(this.images);
  }

  getColor() {

  }

  vegetables: Vegetable[] = [
    { name: 'apple' },
    { name: 'banana' },
    { name: 'strawberry' },
    { name: 'orange' },
    { name: 'kiwi' },
    { name: 'cherry' },
  ];

  drop(event: CdkDragDrop<Vegetable[]>) {
    moveItemInArray(this.vegetables, event.previousIndex, event.currentIndex);
  }

  openSidnav() {
    this.sidenave.toggle();
  }


  submit() {

  }
}
