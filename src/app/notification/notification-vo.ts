export class NotificationVO {
    id: any;
    fromId: any;
    toId: any;
    message: string;
    fromUserName: string;
    taskId: any;
    readTime: any;
    fromUserProfilePicture: string;
}
