import { Component, OnInit, ViewChild, ViewEncapsulation, Output, EventEmitter } from '@angular/core';
import * as Stomp from 'stompjs';
import * as SockJS from 'sockjs-client';
import { ConfirmationDialogComponent } from '../confirmation-dialog/confirmation-dialog.component';
import { NotificationService } from './notification.service';
import { NotificationVO } from './notification-vo';
import { MatMenuTrigger } from '@angular/material/menu';
import { MatDialog } from '@angular/material/dialog';
import { NotificationsDialogBoxComponent } from '../notifications-dialog-box/notifications-dialog-box.component';
import { UserVO } from '../message-passing/user-vo';
import { StompClientService } from '../stomp-client.service';


@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.scss'],
})
export class NotificationComponent implements OnInit {

  constructor(private notficationService: NotificationService, private dialog: MatDialog,
    private stompClientService: StompClientService) { }

  notficationList: any;
  show = false;
  userVO = new UserVO();
  private serverUrl = '/messaging-service/socket';
  isLoaded = false;
  isCustomSocketOpened = false;
  private stompClient;
  enable = false;

  @ViewChild(MatMenuTrigger)
  contextMenu: MatMenuTrigger;

  contextMenuPosition = { x: '0px', y: '0px' };
  @Output() notificationEmiiter: EventEmitter<any> = new EventEmitter<any>();
  @Output() checkNotificationOpenEmiiter: EventEmitter<any> = new EventEmitter<any>();
  @Output() notificationObject: EventEmitter<any> = new EventEmitter<any>();

  notificationId: any;

  onContextMenu(event: MouseEvent) {
    event.preventDefault();
    console.log(event);
    this.contextMenuPosition.x = event.clientX + 'px';
    this.contextMenuPosition.y = event.clientY + 'px';
    console.log(this.contextMenuPosition);
    this.contextMenu.menu.focusFirstItem('mouse');
    this.contextMenu.openMenu();
  }


  ngOnInit(): void {
    // console.log(this.stompClientService.stompClient);
    // this.connectSocketClient();
    if (this.stompClientService.stompClient && this.stompClientService.stompClient.connect === false) {
      console.log('reconnecting web sockets');
      this.stompClientService.initializeWebSocketConnection();
    }
    this.getLoggedUserDetails();
    this.getNotificationsCount();
    this.checkNotificationOpenEmiiter.emit(false);
    this.notificationObject.emit(this);
    this.addSubscription();
  }

  addSubscription() {
    this.stompClientService.reconnecting.subscribe(data => {
      console.error('socket disconnected caught in notification component ');
    });
  }

  // connectSocketClient() {
  //   if (this.stompClientService.stompClient && this.stompClientService.stompClient.connected === false
  //     && !this.stompClientService.isLoaded) {
  //     this.stompClientService.initializeWebSocketConnection();
  //   }
  // }

  updateReadTime() {
    this.notficationService.updateReadTime(this.notificationId).subscribe(data => {
      this.getNotificationsCount();
    });
  }

  openNotifications() {
    const notificationDialogBox = this.dialog.open(NotificationsDialogBoxComponent, {
      disableClose: true,
      width: '547px',
      height: '547px',
      autoFocus: false,
      panelClass: 'custom-dialog-container'
    });
    notificationDialogBox.afterOpened().subscribe(data => {
      this.checkNotificationOpenEmiiter.emit(true);
    });
    notificationDialogBox.afterClosed().subscribe(data => {
      this.checkNotificationOpenEmiiter.emit(false);
    });
    notificationDialogBox.updatePosition({ right: '10px', top: '5px' });
    notificationDialogBox.componentInstance.notificationEmiiter.subscribe(data => {
      console.log('entered notfication');
      this.notificationEmiiter.emit(data.taskId);
    });

    notificationDialogBox.componentInstance.updateReadTimeEmiiter.subscribe(data => {
      if (data === true) {
        this.getNotificationsCount();
      }
    });
  }

  anyAction(notfication) {
    console.log(notfication);
  }

  getNotificationsCount() {
    this.notficationService.getNotificationCount().subscribe(data => {
      this.notficationList = data;
      this.show = true;
    });
  }

  getLoggedUserDetails() {
    // this.connectSocketClient();
    this.notficationService.getLoggedInUserDetails().subscribe(data => {
      // this.connectSocketClient();
      this.userVO = data;
      this.show = this.stompClientService.show;
      setTimeout(() => {
        this.openSocket(this.userVO.userId);
      }, 3000);
    });
  }

  getToken() {
    return localStorage.getItem('token');
  }


  getHeader() {
    const httpOptions = {
      Authorization: this.getToken()
    };
    return httpOptions;
  }

  initializeWebSocketConnection() {
    const ws = new SockJS(this.serverUrl);
    this.stompClient = Stomp.over(ws);
    this.stompClient.debug = null;
    this.stompClient.connect(this.getHeader(), (frame) => {
      this.isLoaded = true;
      // this.openGlobalSocket();
      this.openSocket(this.userVO.userId);
      this.show = true;
    });
  }

  openSocket(userId) {
    if (this.stompClientService.isLoaded) {
      this.isCustomSocketOpened = true;
      console.log('Custom socket opened  in notification component . . . . .');
      this.stompClientService.stompClient.subscribe('/socket-publisher/notification-' + userId, (message) => {
        this.handleResult(message);
      }, this.getHeader());
    }
  }

  handleResult(message) {
    console.log('Recieving message in notification component. . . . ');
    this.notificationId = JSON.parse(message.body).id;
    this.notficationList = this.notficationList + 1;
  }

  userProfile(str) {
    const assignee = str.split(' ');
    for (let i = 0; i < assignee.length; i++) {
      assignee[i] = assignee[i].charAt(0).toUpperCase();
    }
    return assignee.join('');
  }

}
