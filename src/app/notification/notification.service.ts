import { Injectable } from '@angular/core';

import { HttpClient } from '@angular/common/http';
import { NotificationVO } from './notification-vo';

import { UserVO } from '../message-passing/user-vo';
import { ResponseString } from '../message-passing/reponse-vo';
import { UserDetailsVO } from '../notifications-dialog-box/user-details-vo';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {

  constructor(private http: HttpClient) { }

  notificationListUrl = environment.messagingBaseUrl + '/notification/v1/get-list';
  private getLoggedUserDetailsUrl = environment.messagingBaseUrl + '/user-service/v1/get/logged-in/user-details';
  notificationCountUrl = environment.messagingBaseUrl + '/notification/v1/get/count';
  private updateNotificationReadTimeUrl = environment.messagingBaseUrl + '/notification/v1/updated-read-time/';
  private getProfilePictureUrl = environment.messagingBaseUrl + '/notification/v1/get-profile-picture';

  getProfilePictures(userIdList) {
    return this.http.post<UserDetailsVO[]>(this.getProfilePictureUrl, userIdList);
  }
  updateReadTime(notificationId) {
    return this.http.get<ResponseString>(this.updateNotificationReadTimeUrl + notificationId);
  }

  getNotificationList() {
    return this.http.get<NotificationVO[]>(this.notificationListUrl);
  }

  getNotificationCount() {
    return this.http.get<any>(this.notificationCountUrl);
  }


  getLoggedInUserDetails() {
    return this.http.get<UserVO>(this.getLoggedUserDetailsUrl);
  }

}
